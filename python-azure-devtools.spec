%global         srcname         azure-devtools
%global         commit          67d46b9c4292c267c14833b50bb313c077e63cd5
%global         shortcommit     %(c=%{commit}; echo ${c:0:7})
%global         short_version   1.2.1

Name:           python-%{srcname}
Version:        %{short_version}~git.4.%{shortcommit}
Release:    1%{?dist}
Summary:        Microsoft Azure Development Tools for SDK
License:        MIT and Apache-2.0
URL:            https://github.com/Azure/azure-sdk-for-python/
Source0:        azure-devtools-%{commit}.tar.gz
Patch0:         python-azure-devtools-requirements-fix.patch

BuildArch:      noarch

BuildRequires:  python3-devel

%global _description %{expand:
Development tools for Python-based Azure tools
This package contains tools to aid in developing Python-based Azure code.}

%description %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -v -p3 -c -n %{srcname}-%{commit}


%build
%pyproject_wheel


%generate_buildrequires
%pyproject_buildrequires -r


%install
%pyproject_install
%pyproject_save_files azure_devtools

rm -f %{buildroot}%{_bindir}/{perfstress,perfstressdebug,systemperf}


%files -n python3-%{srcname} -f %{pyproject_files}
%doc README.rst
%license LICENSE


%changelog
* Fri Apr 12 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.2.1~git.4.67d46b9-1
- initial build
